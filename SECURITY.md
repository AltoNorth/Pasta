# FrickFad Security Policy

## Supported Versions

This is the Security Policy for PASTA, here you will find what you need to know.

| Version | Supported          |
| ------- | ------------------ |
| 5.1.x   | :white_check_mark: |
| 5.0.x   | :x:                |
| 4.0.x   | :white_check_mark: |
| < 4.0   | :x:                |

## Reporting a Vulnerability

Finding a vulnerability can be scary, if you find a vulnerability you can go to the "Issues" page. We do not accept emails because of a vulnerability,
all vulunerabilities or issues must be reported in the "Issues" page so that we can really see it..


