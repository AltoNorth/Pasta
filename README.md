Pasta is a web browser project to be released to the public, it's mainly based for YMSH but anyone could use it, it's been developed with Visual Studio Community 2019 as an open source web project. 

Pasta won't collect data, we don't do that here at YMSH. So feel free to browse safely without a corporation-owned web browser.



### A message from the creator

Hi, I am SomnusLux, you can call me Somnus. I created this web browser because I thought it would be good for the rest of the YMSH. And possibly other people out there, I hope you put this to good use.  And we have not yet made Pasta plug-in compatible, so plug-ins like Java won't work. In Pasta's lifetime we will do our best to find out a way to use Java within Pasta. 

### Support

If you have any trouble, you can contact us through our issues page and we will do our best to fix the problem.
